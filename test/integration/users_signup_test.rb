require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
     get signup_path
     assert_no_difference 'User.count' do
       post users_path, user: { name: "", email: "user@invalid", password: "foo", password_confirmation: "bar" }
     end
     assert_template 'users/new'
  end
  
  test "valid signup information" do
     get signup_path
     assert_difference 'User.count', 1 do
       post users_path, user: { name: "rodrigo", email: "user@valid.com", password: "valid", password_confirmation: "valid" }
     end
     assert_template 'users/show'
  end
end
